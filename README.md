# eventsourced Sentry trace handler

[![GoReportCard](https://goreportcard.com/badge/gitlab.com/unboundsoftware/eventsourced/sentry)](https://goreportcard.com/report/gitlab.com/unboundsoftware/eventsourced/sentry)
[![GoDoc](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/sentry?status.svg)](https://godoc.org/gitlab.com/unboundsoftware/eventsourced/sentry)
[![Build Status](https://gitlab.com/unboundsoftware/eventsourced/sentry/badges/main/pipeline.svg)](https://gitlab.com/unboundsoftware/eventsourced/sentry/commits/main)
[![coverage report](https://gitlab.com/unboundsoftware/eventsourced/sentry/badges/main/coverage.svg)](https://gitlab.com/unboundsoftware/eventsourced/sentry/commits/main)

Package `sentry` provides a [Sentry](https://sentry.io) implementation of the trace handler which can be used with the [eventsourced framework](https://gitlab.com/unboundsoftware/eventsourced/eventsourced).

Download:
```shell
go get gitlab.com/unboundsoftware/eventsourced/sentry
```




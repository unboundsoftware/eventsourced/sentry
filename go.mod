module gitlab.com/unboundsoftware/eventsourced/sentry

go 1.21

toolchain go1.24.1

require (
	github.com/getsentry/sentry-go v0.31.1
	github.com/stretchr/testify v1.10.0
	gitlab.com/unboundsoftware/eventsourced/eventsourced v1.18.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.18.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
